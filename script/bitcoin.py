from keras.models import Sequential,model_from_json
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras.layers import Activation
import numpy as np
import argparse
import os
import glob
import pandas as pd
import matplotlib.pyplot as plt
import keras
from keras.callbacks import Callback
import tensorflow as tf
from azureml.core import Run
from sklearn import preprocessing

run = Run.get_context()
dataset = run.input_datasets['bitcoin']
data = dataset.to_pandas_dataframe()

data['mean'] = data.iloc[:, [4,5,6,7]].mean(axis=1)

data = data.set_index('Date')
#data = data[['mean']]
data['mean'].fillna(method='ffill', inplace=True)
data['mean_predict_month']=btc['mean'].shift(-30)
data=data.drop(['SNo','Name','Symbol'],axis=1)
#data=preprocessing.scale(data)

index = data.index
number_of_rows = len(index)

train_data = data.iloc[:(number_of_rows-30)]
test_data = data.iloc[-30:]

training_set = train_data.values
training_set = np.reshape(training_set, (len(training_set),1, 6))

from sklearn.preprocessing import MinMaxScaler
import sklearn
import sklearn.model_selection

X_train=train_data.drop('mean_predict_month',axis=1)
Y_train=btc['mean_predict_month']

X_train, X_valid, Y_train, Y_valid = sklearn.model_selection.train_test_split(X_train, Y_train, test_size=0.3)

model = Sequential()
model.add(LSTM(128,activation="sigmoid",input_shape=(1,6)))
model.add(Dropout(0.2))
model.add(Dense(1))
model.compile(loss='mean_squared_error',
              optimizer='adam',
              metrics=['accuracy'])




class LogRunMetrics(Callback):
    def on_epoch_end(self, epoch, log):
        run.log('Loss', log['val_loss'])
        run.log('Accuracy', log['val_accuracy'])


history = model.fit(X_train, Y_train,
                    batch_size=10,
                    epochs=100,
                    verbose=2,
					validation_data=(X_valid, Y_valid),
                    callbacks=[LogRunMetrics()])


test_set = test_data.values
#inputs = np.reshape(test_set, (len(test_set), 1))
inputs = np.reshape(inputs, (len(inputs), 1, 6))
predicted_BTC_price = model.predict(inputs)
#predicted_BTC_price = sc.inverse_transform(predicted_BTC_price)

test_data['mean_predict'] = predicted_BTC_price

plt.figure(figsize=(6, 3))
plt.title('Bitcoin prediction ({} epochs)'.format(100), fontsize=14)
plt.plot(test_data['mean'], 'b-', label='real price', lw=4, alpha=0.5)
plt.plot(test_data['mean_predict'], 'r--', label='predict price', lw=4, alpha=0.5)
plt.legend(fontsize=12)
plt.grid(True)

run.log_image('Test data', plot=plt)
score = model.evaluate(X_valid, Y_valid, verbose=0)
run.log("Final test loss", score[0])
print('Test loss:', score[0])

run.log('Final test accuracy', score[1])
print('Test accuracy:', score[1])

plt.figure(figsize=(6, 3))
plt.title('Bitcoin prediction ({} epochs)'.format(100), fontsize=14)
plt.plot(history.history['val_accuracy'], 'b-', label='Accuracy', lw=4, alpha=0.5)
plt.plot(history.history['val_loss'], 'r--', label='Loss', lw=4, alpha=0.5)
plt.legend(fontsize=12)
plt.grid(True)

# log an image
run.log_image('Accuracy vs Loss', plot=plt)

os.makedirs('./outputs/model', exist_ok=True)

model_json = model.to_json()

with open('./outputs/model/model.json', 'w') as f:
    f.write(model_json)
	
model.save_weights('./outputs/model/model.h5')
print("model saved in ./outputs/model folder")