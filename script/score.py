import json
import numpy as np
import os
from tensorflow.keras.models import model_from_json
from azureml.core.model import Model

def init():
	global model
    
	model_root = Model.get_model_path('bitcoin')
	json_file = open(os.path.join(model_root, 'model.json'), 'r')
	model_json = json_file.read()
	json_file.close()
	model = model_from_json(model_json)
	# load weights into new model
	model.load_weights(os.path.join(model_root, "model.h5"))   
	model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])

def run(raw_data):
	data = json.loads(raw_data)['data']
	data = np.array(data, dtype=np.float32)
	result=model.predict(data)
	result = result.tolist()
	return {"result": result}
